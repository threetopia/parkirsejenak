<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin</title>
<link rel="stylesheet" type="text/css" href="../css/admin.css" />
</head> 
<body>
<?php
require_once("../db/database.php");
$database = new database;	
?>
<script type="text/javascript">
function AddData()
{
	location.href='addbanner.php';
}

function cekForm()
{
  with(document.frm) ;
}

</script>
    <div id="wraplogin">
    	<?php
		require("menuatas.php");
        ?>
    	
        <form name="frm" action="savebanner.php" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend><strong>Add Banner</strong></legend>
            <table width="100%" border="0" >
                <tr>
                    <td>Tipe</td>
                    <td>:</td>
                    <td>
                    <?php
                    $tipe = $_GET['tipe'];
					$subtipe = $_GET['subtipe'];
					?>       
                   	<select name="tipe" onChange="location.href = 'addbanner.php?subtipe=<?php echo $subtipe?>&amp;tipe=' + this.options[this.selectedIndex].value;">
                    	<option value="0" <?php echo(($tipe==0)?'selected':'')?>>-</option>
                        <option value="1" <?php echo(($tipe==1)?'selected':'')?>>Pondok Renungan</option>
                        <option value="2" <?php echo(($tipe==2)?'selected':'')?>>Pondok Doa</option>
                        <option value="3" <?php echo(($tipe==3)?'selected':'')?>>Pondok Anak</option>
                        <option value="4" <?php echo(($tipe==4)?'selected':'')?>>Pondok Kita</option>
                        <option value="5" <?php echo(($tipe==5)?'selected':'')?>>Papan Pengumanan</option>
                        <option value="6" <?php echo(($tipe==6)?'selected':'')?>>DAUK</option>
                        <option value="7" <?php echo(($tipe==7)?'selected':'')?>>Member</option>
                        <option value="8" <?php echo(($tipe==8)?'selected':'')?>>Contact Us</option>
                        <option value="99" <?php echo(($tipe==99)?'selected':'')?>>Other</option>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>Sub Tipe</td>
                    <td>:</td>
                    <td>              
                    <select name="subtipe" onChange="location.href = 'addbanner.php?tipe=<?php echo $tipe?>&amp;subtipe=' + this.options[this.selectedIndex].value;">
						<option value="0" <?php echo(($subtipe==0)?'selected':'')?>>-</option>
                        <option value="1" <?php echo(($subtipe==1)?'selected':'')?>>Home</option>
                        <option value="2" <?php echo(($subtipe==2)?'selected':'')?>>Background</option>
                        <option value="3" <?php echo(($subtipe==3)?'selected':'')?>>Icon</option>
                        <option value="4" <?php echo(($subtipe==4)?'selected':'')?>>Musik</option>
                        <option value="5" <?php echo(($subtipe==5)?'selected':'')?>>Kiri</option>
                        <option value="6" <?php echo(($subtipe==6)?'selected':'')?>>Movie</option>
                        <option value="7" <?php echo(($subtipe==7)?'selected':'')?>>Icon Home</option>
                    </select>
                    </td>
                </tr>  
                <?php if ($subtipe == 1 && $subtipe==3) { ?>
				<tr>
                    <td>Inisial</td>
                    <td>:</td>
                    <td><input type="text" name="txtinisial" value="" size="60" /></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td>:</td>
                    <td><input type="text" name="txttitle" value="" size="60" /></td>
                </tr>
               	<tr>
                    <td>Link</td>
                    <td>:</td>
                    <td><input type="text" name="txtlink" value="" size="60" /></td>
                </tr>
                <?php }elseif ($subtipe == 6){ ?>
                <tr>
                    <td>Link</td>
                    <td>:</td>
                    <td><input type="text" name="txtlink" value="" size="60" /></td>
                </tr>
                
				<?php } ?>
			
                
                <tr>
                    <td valign="top">Banner</td>
                    <td>:</td>
                    <td><input type="file" name="uploaded_file" value="" /><br/><strong style="color:#FF0000;">Keterangan Gambar :</strong><br/>
                    <?php 
					if ($subtipe ==1){
						echo "<strong style='color:#FF0000;'>Gambar Akan Tampil di bagian Home<br/>Ukuran Gambar : H : 150px , W : 293px</strong>";
					}elseif($subtipe ==2){
						echo "<strong style='color:#FF0000;'>Gambar Akan Tampil di bagian Background Kanan<br/>Ukuran Gambar : H : 600px , W : 700px</strong>";
					}elseif($subtipe ==3){
						echo "<strong style='color:#FF0000;'>Gambar Akan Tampil di bagian Menu<br/>Ukuran Gambar : H : 60px , W : 60px</strong>";
					}elseif($subtipe ==4){
						echo "";
					}elseif($subtipe ==5){
						echo "<strong style='color:#FF0000;'>Gambar Akan Tampil di bagian Background Kiri<br/>Ukuran Gambar : H : 600px , W : 475px</strong>";
					}elseif($subtipe ==6){
						echo "<strong style='color:#FF0000;'>Link di isi dengan link Movie</strong>";
					}elseif($subtipe ==7){
						echo "<strong style='color:#FF0000;'>Gambar Akan Tampil di bagian Home Icon<br/>Ukuran Gambar : H : 150px , W : 212px</strong>";
					}else{
						echo "<strong style='color:#FF0000;'>Gambar Akan Tampil di bagian Home Icon<br/>Ukuran Gambar : H : 150px , W : 212px</strong>";
					} ?></td>
                </tr>  
				<tr>
                    <td>Color</td>
                    <td>:</td>
                    <td><input type="text" name="txtcolor" value="" size="60" /></td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><input type="submit" name="submit" value="submit" /></td>
                </tr>               
            </table>
           
        </fieldset>
        </form>
    </div>
</body>
</html>