<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin</title>
<link rel="stylesheet" type="text/css" href="../css/admin.css" />
</head> 
<body>
<?php
require_once("../db/database.php");
$database = new database;	
$idnews = $_GET['idnews'];
 
?>
<script type="text/javascript" src="tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",

        // Example content CSS (should be your site CSS)
        content_css : "css/example.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "js/template_list.js",
        external_link_list_url : "js/link_list.js",
        external_image_list_url : "js/image_list.js",
        media_external_list_url : "js/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
                username : "Some User",
                staffid : "991234"
        }
});
</script>
<script type="text/javascript">
function AddData()
{
	location.href='addnews.php';
}

function cekForm()
{
  with(document.frm) ;
}

</script>
    <div id="wraplogin">
    	<?php
		require("menuatas.php");
		$query = "SELECT * FROM t_news WHERE id_news = '".$idnews."'";
		$data = $database->getData($query);
		$subtipe = 	$data[0]['subtipe'];
		$tipe = $data[0]['tipe'];
		$vjudul = $data[0]['vjudul'];
		$visi = $data[0]['visi'];
        ?>
    	
        <form name="frm" action="editsave.php" method="post" >
        <input type="hidden" name="idnews" value="<?php echo $idnews ?>">
        <fieldset>
            <legend><strong>Add Artikel</strong></legend>
            <table width="100%" border="0" >
                <tr>
                    <td>Tipe</td>
                    <td>:</td>
                    <td>      
                   	<select name="tipe" onChange="location.href = 'addnews.php?subtipe=<?php echo $subtipe?>&amp;tipe=' + this.options[this.selectedIndex].value;">
                    	<option value="0" <?php echo(($tipe==0)?'selected':'')?>>-</option>
                        <option value="1" <?php echo(($tipe==1)?'selected':'')?>>Pondok Renungan</option>
                        <option value="2" <?php echo(($tipe==2)?'selected':'')?>>Pondok Doa</option>
                        <option value="3" <?php echo(($tipe==3)?'selected':'')?>>Pondok Anak</option>
                        <option value="4" <?php echo(($tipe==4)?'selected':'')?>>Pondok Kita</option>
                        <option value="5" <?php echo(($tipe==5)?'selected':'')?>>Papan Pengumanan</option>
                        <option value="6" <?php echo(($tipe==6)?'selected':'')?>>DAUK</option>
                        <option value="7" <?php echo(($tipe==7)?'selected':'')?>>Member</option>
                        <option value="8" <?php echo(($tipe==8)?'selected':'')?>>Contact Us</option>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>Sub Tipe</td>
                    <td>:</td>
                    <td>
                    
                    <select name="subtipe" onChange="location.href = 'addnews.php?tipe=<?php echo $tipe?>&amp;subtipe=' + this.options[this.selectedIndex].value;">
                    	<?php
                        $query = "SELECT * FROM t_tipe_news WHERE tipe = '".$tipe."'";
						$data = $database->getData($query);
						if(!empty($data))
						{								
							foreach ($data as $val){
								echo ' <option value="'.$val['subtipe'].'" '.(($subtipe==$val['subtipe'])?'selected':'').'>'.$val['vnama'].'</option>';
							}
						}else{
							echo '<option value="0">Belum Ada Data</option>';
						}
						?>
                    </select>
                    </td>
                </tr>  
                <tr>
                    <td>Judul</td>
                    <td>:</td>
                    <td><input type="text" name="txtjudul" value="<?php echo $vjudul?>" size="101" /></td>
                </tr>  
                <tr>
                    <td valign="top">Isi</td>
                    <td valign="top">:</td>
                    <td><textarea name="txtisi" cols="60" rows="40"><?php echo $visi?></textarea></td>
                </tr> 
                <tr>
                    <td colspan="3" align="center"><input type="submit" name="submit" value="submit" /></td>
                </tr>               
            </table>
        </fieldset>
        </form>
    </div>
</body>
</html>