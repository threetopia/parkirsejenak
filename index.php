<?php error_reporting(E_ALL ^ E_NOTICE);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Parkir Sejenak</title>
<link rel="stylesheet" type="text/css" href="css/home.css" />
<link rel="stylesheet" type="text/css" href="css/addon.css" />
<?php
require_once("db/database.php");
$database = new database;	
$query = "SELECT * FROM t_banner Where id_subtipe=1";
$data = $database->getData($query);
if(!empty($data))
{	
	for ($i=0;$i<=7;$i++)
	{
		$no = $i + 1;
		$file_name[$no] = $data[$i][file_name];
		$vtitle[$no] = $data[$i][vtitle];                                                                            
		$vlink[$no] = $data[$i][vlink];
		$vcolor[$no] = $data[$i][vcolor];
	}
}
$divtambah = "style=display:inline-block;background:#FFFFF;height:36px;padding:0;margin:0;";
//banner
$query2 = "SELECT * FROM t_banner Where id_subtipe=6 AND id_tipe=99";
$data2 = $database->getData($query2);
if(!empty($data2))
{	
	$vlink2 = $data2[0][vlink];
	
}
//banner
$query3 = "SELECT * FROM t_banner Where id_subtipe=7 AND id_tipe=99";
$data3 = $database->getData($query3);
if(!empty($data3))
{	
	$vbannerhome ="admin/images/iconhome/".$data3[0][file_name];
	
}
?>
<!--<script type="text/javascript" src="js/jssnow.js"></script>-->
<style type="text/css">
body{
	background:white;
}
	.pondokrenung{
		background:url(admin/images/home/<?php echo $file_name[1]?>);
		color:#FF0000;
	}
			.pondokrenung a span.wspan{
				color:<?php echo $vcolor[1]?>;
				padding-top:50px;
			}
	.pondokdoa{
		background:url(admin/images/home/<?php echo $file_name[2]?>);
	}
		.pondokdoa a span.wspan{
			color:<?php echo $vcolor[2]?>;
			padding-top:50px;
		}
	.pondokanak{
		background:url(admin/images/home/<?php echo $file_name[3]?>);
	}
		.pondokanak a span.wspan{
			color:<?php echo $vcolor[3]?>;
			padding-top:50px;
		}
	.pondokkita{
		background:url(admin/images/home/<?php echo $file_name[4]?>);
	}	
		.pondokkita a span.wspan{
			color:<?php echo $vcolor[4]?>;
			padding-top:50px;
		}	
	.papanpengumuman{
		background:url(admin/images/home/<?php echo $file_name[5]?>);
	}
		.papanpengumuman a span.wspan{
			color:<?php echo $vcolor[5]?>;
			padding-top:50px;
		}
	.dauk{
		background:url(admin/images/home/<?php echo $file_name[6]?>);
	}
		.dauk a span.wspan{
			color:<?php echo $vcolor[6]?>;
			padding-top:50px;
		}
	.member{
		background:url(admin/images/home/<?php echo $file_name[7]?>);
	}
		.member a span.wspan{
			color:<?php echo $vcolor[7]?>;
			padding-top:50px;
		}
	.contactus{
		background:url(admin/images/home/<?php echo $file_name[8]?>);
	}
		.contactus a span.wspan{
			color:<?php echo $vcolor[8]?>;
			padding-top:50px;
		}

</style>
</head>
<body>

<div id="content">
	<div id="top">
    	<div id="sitetitle">
        	<h1>Parkir Sejenak</h1>
        </div>
    </div>
	<div id="mainmenu">
    	<ul class="menu">
        	<li class="has-sub pondokrenung"><a href="<?php echo $vlink[1]?>" title="<?php echo $vtitle[1]?>"><span class="wspan"><?php echo $vtitle[1]?></span></a>
            	<ul>
                	<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=1&amp;news=1"><span>Reflection</span></a></li>
					<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=1&amp;news=2"><span>Words Of Wisdom</span></a></li>
                    <li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=1&amp;news=3"><span>Retreat</span></a></li>
                </ul>          
            </li>
            <li class="has-sub pondokdoa"><a href="<?php echo $vlink[2]?>" title="<?php echo $vtitle[2]?>"><span class="wspan"><?php echo $vtitle[2]?></span></a>
            	<ul>
                	<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=2&amp;news=4"><span>Yesus Kristus </span></a></li>
					<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=2&amp;news=5"><span>Bunda Maria</span></a></li>
                    <li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=2&amp;news=6"><span>Santo / Santa</span></a></li>
                </ul>
            </li>
            <li class="papanpengumuman"><a href="<?php echo $vlink[5]?>" title="<?php echo $vtitle[5]?>"><span class="wspan"><?php echo $vtitle[5]?></span></a></li>
            <li class="member"><a href="<?php echo $vlink[7]?>" title="<?php echo $vtitle[7]?>"><span class="wspan"><?php echo $vtitle[7]?></span></a></li>
            <li class="has-sub pondokanak"><a href="<?php echo $vlink[3]?>" title="<?php echo $vtitle[3]?>"><span class="wspan"><?php echo $vtitle[3]?></span></a>
            	<ul>
                	<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=3&amp;news=7"><span>Kisah</span></a></li>
					<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=3&amp;news=8"><span>Bermain Bersama</span></a></li>
                    <li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=3&amp;news=9"><span>Sahabat</span></a></li>
                </ul>
            </li>
            <li class="has-sub pondokkita"><a href="<?php echo $vlink[4]?>" title="<?php echo $vtitle[4]?>"><span class="wspan"><?php echo $vtitle[4]?></span></a>
            	<ul>
                	<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=4&amp;news=10"><span>Info</span></a></li>
					<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=4&amp;news=11"><span>Musik</span></a></li>
                    <li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=4&amp;news=12"><span>Buku</span></a></li>
               		<li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=4&amp;news=13"><span>Film</span></a></li>
                    <li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=4&amp;news=14"><span>Benda Rohani</span></a></li>
                    <li class="has-sub" <?php //echo $divtambah?>><a href="news/index.php?tipe=4&amp;news=15"><span>Lain-lain</span></a></li>
                </ul>
            </li>
            <li class="dauk"><a href="<?php echo $vlink[6]?>" title="<?php echo $vtitle[6]?>"><span class="wspan"><?php echo $vtitle[6]?></span></a></li>
            <!--<li class="contactus"><a href="<?php echo $vlink[8]?>" title="<?php echo $vtitle[8]?>"><span class="wspan"><?php echo $vtitle[8]?></span></a></li>-->
        </ul>
    </div>
    <div id="maincontent">
    	<embed class="youtube-player" width="800" height="400" src="<?php echo $vlink2?>" type="application/x-shockwave-flash"></embed>
    </div>
    <div class="btnfb" style="display:inline;float:right;width:50px;height:50px;clear:none;margin:10px 120px 0px 0px;">
        <a href="http://www.facebook.com/parkirsejenak.sejenak" target="_blank" title="Facebook Parkir Sejenak"><img style="height:50px;width:50px;" src="images/fb.jpg" /></a>
    </div>
    <div class="btntw" style="display:inline;float:right;width:50px;height:50px;clear:none;margin:10px 0px 0px 0px;">
        <a href="https://twitter.com/parkirsejenak" target="_blank" title="Twetter Parkir Sejenak"><img style="height:50px;width:50px;" src="images/twitter.jpg" /></a>
    </div>
    <div id="copyright">
    	<span>Copyright 2013 - parkirsejenak.com<span>
    </div>
    
</div>

</body>
</html>