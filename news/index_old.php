<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pondok Renungan</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script type="text/javascript" src="../js/jssnow.js"></script>
<style type="text/css">
body{
	background:#000;
}
</style>
<script type="text/javascript">
function berita_chkString(str)
{
	$str = str_replace(str, vbCrLf,"<br/>");
	$str = str_replace(str, vbCr,"<br/>");
	$str = str_replace(str, vbLf,"<br/>");
	$berita_chkString = $str;
}
//function berita_chkString(str)	 
//    ' Replace CR & LF with <BR>
//    str = str_Replace(str, vbCrLf,"<br />")
//    str = str_Replace(str, vbCr,"<br />")
//    str = str_Replace(str, vbLf,"<br />")
//    
//    berita_chkString = str
//end function
</script>
</head>
<body>
<?php 
require_once("../db/database.php");
$database = new database;	
$tipe = $_GET['tipe'];

if ($_GET['subid'] == "" ){
	$idnews = 0;
}else{
	$idnews = $_GET['subid'];
}
if ($_GET['news'] == "" ){
	$news = 0;
}else{
	$news = $_GET['news'];
}



echo '<ul id="menuatas">';
	echo '<li style="border:none;"><a href="../index.php" title="Home"><img style="border:none;" src="../images/btn_home.png" /></a></li>';
    echo '<li><a href="../news/index.php?tipe=1" title="Pondok Renungan" '.(($tipe==1)?'class="select"':'').'>Pondok Renungan</a></li>';
    echo '<li><a href="../news/index.php?tipe=2" title="Pondok Doa" '.(($tipe==2)?'class="select"':'').'>Pondok Doa</a></li>';
    echo '<li><a href="../news/index.php?tipe=3" title="Pondok Anak" '.(($tipe==3)?'class="select"':'').'>Pondok Anak</a></li>';
    echo '<li><a href="../news/index.php?tipe=4" title="Pondok Kita" '.(($tipe==4)?'class="select"':'').'>Pondok Kita</a></li>';
    echo '<li><a href="../news/index.php?tipe=5" title="Papan Pengumuman" '.(($tipe==5)?'class="select"':'').'>Papan Pengumuman</a></li>';
    echo '<li><a href="../news/index.php?tipe=6" title="Dari Aku Untuk Kamu" '.(($tipe==6)?'class="select"':'').'>DAUK</a></li>';
    echo '<li><a href="../news/index.php?tipe=7" title="Member" '.(($tipe==7)?'class="select"':'').'>Member</a></li>';
    echo '<li style="border:none;"><a href="../news/index.php?tipe=8" title="Contact Us" '.(($tipe==8)?'class="select"':'').'>Contact Us</a></li>';
echo '</ul>';
?>


<form name="frm" action="" method="get">
<div id="contentisi">
	<?php if ($tipe==10)   { //if (($tipe==6) || ($tipe==7) || ($tipe==8) )  {?>
    <div class="wrapcenter">
    <h2>Dari Aku Untuk Kamu</h2>
    <p align="center" style="width:100%;">Halooo,
Apabila kalian menyukai website ini dan ingin terus menikmati website ini selanjutnya, maka kalian dapat ikut berpartisipasi. Mau tau ?

Tidak bisa dipungkiri lagi bahwa kemajuan teknologi berkembang dengan pesat. Kemajuan Teknologi memudahkan kita untuk mencari beragam informasi. Hal ini membuat kami berpikir lebih kreatif lagi untuk memberikan pelayanan kepada umat.Seperti yang kita tau dalam sebuah pelayanan, banyak sekali pihak yang terlibat dari umat sendiri, biarawan/wati lapisan masyarakat secara luas tanpa memandang SARA juga sangat berperan. Dan semuanya memerlukan pembiayaan yang tidak sedikit.

Maka dari itu saya menggunakan sistem baru ini , dimana Anda pun bisa ikut berperan tanpa memberatkan.

Apabila anda menyukai website ini, please donasikan BERAPAPUN yang anda pikir layak dan bisa untuk anda donasikan. Seandai-nyaanda tidak ber-donasi-pun tidak apa apa. TIDAK ADA PAKSAAN. Lakukan lah sesuai kata hati nurani anda. Saya yakin cara ini bisa membantu perkembangan pelayanan di Indonesia. Hasil donasi anda nanti akan bisa dipakai lagi untuk memproduksi karya kami selanjutnya. Sekali lagi, semua ini TANPA PAKSAAN ^ ^ berapapun donasi anda, kami terima dengan senang hati. Tuhan Memberkati.

Alamat donasi :

Bank Mandiri Cab. Bursa Efek Jakarta 
a/c 104 - 000 - 412623 - 6
a/n Erika

God Bless You, Love..

 

Admin</p>
    </div>
	<?php }else{?>	
	<div class="wrapkanan">
        <ul>
        	<?php  
			if($tip == 1){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=1" class="select2"><span class="bingkai">Pondok Renungan</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=1" '.(($news==1)?'class="select"':'').' title="Reflection">Reflection</a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=2" '.(($news==2)?'class="select"':'').' title="Words Of Wisdom">Words Of Wisdom</a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=3" '.(($news==3)?'class="select"':'').' title="Retreat">Retreat</a></li>';
			}elseif ($tipe == 2){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=2" class="select2"><span class="bingkai">Pondok Doa</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=2&amp;news=4" '.(($news==4)?'class="select"':'').' title="Yesus Kristus">Yesus Kristus</a></li>';
				echo '<li><a href="../news/index.php?tipe=2&amp;news=5" '.(($news==5)?'class="select"':'').' title="Bunda Maria">Bunda Maria</a></li>';
				echo '<li><a href="../news/index.php?tipe=2&amp;news=6" '.(($news==6)?'class="select"':'').' title="Santo / Santa">Santo / Santa</a></li>';
			}elseif ($tipe == 3){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=3" class="select2"><span class="bingkai">Pondok Anak</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=3&amp;news=7" '.(($news==7)?'class="select"':'').' title="Kisah">Kisah</a></li>';
				echo '<li><a href="../news/index.php?tipe=3&amp;news=8" '.(($news==8)?'class="select"':'').' title="Bermain Bersama">Bermain Bersama</a></li>';
				echo '<li><a href="../news/index.php?tipe=3&amp;news=9" '.(($news==9)?'class="select"':'').' title="Sahabat">Sahabat</a></li>';
			}elseif ($tipe == 4){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=4" class="select2"><span class="bingkai">Pondok Kita</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=10" '.(($news==10)?'class="select"':'').' title="Info">Info</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=11" '.(($news==11)?'class="select"':'').' title="Musik">Musik</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=12" '.(($news==12)?'class="select"':'').' title="Buku">Buku</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=13" '.(($news==13)?'class="select"':'').' title="Film">Film</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=14" '.(($news==14)?'class="select"':'').' title="Benda Rohani">Benda Rohani</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=15" '.(($news==15)?'class="select"':'').' title="Lain-lain">Lain-lain</a></li>';
			}elseif ($tipe==5){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=5" class="select2"><span class="bingkai">Papan Pengumuman</span><span class="bingkairight">&nbsp;</span></a></li>';
			}elseif ($tipe==6){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=6" class="select2"><span class="bingkai">Dari Aku Untuk Kamu</span><span class="bingkairight">&nbsp;</span></a></li>';
			}elseif ($tipe==7){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=7" class="select2"><span class="bingkai">Member</span><span class="bingkairight">&nbsp;</span></a></li>';
			}elseif ($tipe==8){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=8" class="select2"><span class="bingkai">Contact Us</span><span class="bingkairight">&nbsp;</span></a></li>';
			}else{
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=1" class="select2"><span class="bingkai">Pondok Renungan</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=1" '.(($news==1)?'class="select"':'').' title="Reflection">Reflection</a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=2" '.(($news==2)?'class="select"':'').' title="Words Of Wisdom">Words Of Wisdom</a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=3" '.(($news==3)?'class="select"':'').' title="Retreat">Retreat</a></li>';
			}
			?>
        	
        </ul>
        <ul class="list">
        	<?php 
			$querytipe = "SELECT * FROM t_news WHERE tipe = '".$tipe."'";
			$datatipe = $database->getData($querytipe);
			if(!empty($datatipe))
			{
				if ($news == ""){
					$news = $datatipe[0]['subtipe'];
				}else{
					$news = $_GET['news'];
				}
			}else{
				$news = 0;
			}
			//echo $querytipe;
			
			$query = "SELECT * FROM t_news WHERE subtipe = '".$news."' AND tipe ='".$tipe."'";
			$data = $database->getData($query);
			//echo $query;
			if(!empty($data))
			{		
				if ($idnews == ""){
					$idnews = $data[0]['id_news'];
				}else{
					$idnews = $_GET['subid'];
				}
				
				foreach ($data as $val)
				{
					echo '<li><a href="index.php?tipe='.$tipe.'&amp;news='.$news.'&amp;subid='.$val['id_news'].'" title="'.$val['vjudul'].'" '.(($val['id_news']==$idnews)?'class="select"':'').'>'.$val['vjudul'].'</a></li>';
				}
			}else{
				echo '<li>Belum Ada Data</li>';
			}
			?>
        </ul>
       <!-- <div style="display:inline-block;color:#F00;font-size:24px;margin-top:20px;text-align:center;width:100%;">1 <span style="color:#060">2</span> 3 4 5</div>-->
    </div>
    <div class="wrapkiri">
    	<?php 
        $query = "SELECT * FROM t_news WHERE subtipe = '".$news."' AND id_news = '".$idnews."'";
		$data2 = $database->getData($query);
		//echo $query ;
		if(!empty($data2))
		{
			//'.$data2[0]['visi'].'
			echo '<h2>'.$data2[0]['vjudul'].'</h2>';
			echo '<p>'.str_replace("\r\n","<br>",$data2[0]['visi']).'</p>';
		}else{
			echo '<h2>Belum Ada Data</h2>';
		}
		?>
    </div>
    <!--<div align="center"><a href="#" title="Previous"><img src="../images/prev.png" width="50" /></a> &nbsp;&nbsp; <a href="#" title="Next"><img src="../images/next.png" width="50" /></a></div>-->

</div>
<?php }?>
</form>
</body>
</html>