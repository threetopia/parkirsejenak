<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php

require_once("../db/database.php");
$database = new database;	
$tipe = $_GET['tipe'];

if ($tipe == 1){
	$vtitle = 'Pondok Renungan';
}elseif ($tipe == 2){
	$vtitle = 'Pondok Doa';
}elseif ($tipe == 3){
	$vtitle = 'Pondok Anak';
}elseif ($tipe == 4){
	$vtitle = 'Pondok Kita';
}elseif ($tipe == 5){
	$vtitle = 'Papan Pengumuman';
}elseif ($tipe == 6){
	$vtitle = 'Dari Aku Untuk Kamu';
}elseif ($tipe == 7){
	$vtitle = 'Member';
}elseif ($tipe == 8){
	$vtitle = 'Contact Us';
}else{
	$vtitle = 'Pondok Renungan';
}

?>
<title><?php echo $vtitle?></title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<!--<script type="text/javascript" src="../js/jssnow.js"></script>-->
<style type="text/css">
body{
	background:#000;
}
span#realnameerror,
span#TglLahirerror,
span#Alamaterror,
span#phonenoerror,
span#Pesanerror,
span#emailerror
{
color:#C10000;
font-size:13px;
}
.fullbg{
	width: 97%;
}
</style>
<script type="text/javascript">
function berita_chkString(str)
{
	$str = str_replace(str, vbCrLf,"<br/>");
	$str = str_replace(str, vbCr,"<br/>");
	$str = str_replace(str, vbLf,"<br/>");
	$berita_chkString = $str;
}
</script>

<!--validasi member-->
<script type="text/javascript">
function checkName(form){
	var eobj=document.getElementById('realnameerror');
	var sRealName = form.txtnama.value;
	var oRE = /^[a-z0-9]+[_.-]?[a-z0-9]+$/i;
	var error=false;
	eobj.innerHTML='';
	if (sRealName == '') {
		error='Nama tidak boleh kosong';
		form.txtnama.focus();
	}
	else if (sRealName.length < 4)
	{
		error="UserName harus lebih dari 4 karakter";
	}
//	else if (!oRE.test(sRealName))
//	{
//		error="Salah format";
//	}
//	
	if (error)
	{
		form.txtnama.focus();
		eobj.innerHTML=error;
		return false;
	}
	return true;
}

function checkTglLahir(form){
	var eobj=document.getElementById('TglLahirerror');
	var sTglLahir = form.txttempat.value;
	var oRE = /^[a-z0-9]+[_.-]?[a-z0-9]+$/i;
	var error=false;
	eobj.innerHTML='';
	if (sTglLahir == '') {
		error='Tempat Lahir tidak boleh kosong';
		form.txttempat.focus();
	}
	else if (sTglLahir.length < 4)
	{
		error="UserName harus lebih dari 4 karakter";
	}
//	else if (!oRE.test(sTglLahir))
//	{
//		error="Salah format";
//	}
	
	if (error)
	{
		form.txttempat.focus();
		eobj.innerHTML=error;
		return false;
	}
	return true;
}

function checkAlamat(form){
	var eobj=document.getElementById('Alamaterror');
	var sTglLahir = form.txtalamat.value;
	var oRE = /^[a-z0-9]+[_.-]?[a-z0-9]+$/i;
	var error=false;
	eobj.innerHTML='';
	if (sTglLahir == '') {
		error='Alamat tidak boleh kosong';
		form.txtalamat.focus();
	}
	else if (sTglLahir.length < 4)
	{
		error="Alamat harus lebih dari 4 karakter";
	}
//	else if (!oRE.test(sTglLahir))
//	{
//		error="Salah format";
//	}
	
	if (error)
	{
		form.txtalamat.focus();
		eobj.innerHTML=error;
		return false;
	}
	return true;
}

function checkPesan(form){
	var eobj=document.getElementById('Pesanerror');
	var sTglLahir = form.txtpesan.value;
	var oRE = /^[a-z0-9]+[_.-]?[a-z0-9]+$/i;
	var error=false;
	eobj.innerHTML='';
	if (sTglLahir == '') {
		error='Pesan tidak boleh kosong';
		form.txtpesan.focus();
	}
	else if (sTglLahir.length < 4)
	{
		error="Pesan harus lebih dari 4 karakter";
	}
//	else if (!oRE.test(sTglLahir))
//	{
//		error="Salah format";
//	}
	
	if (error)
	{
		form.txtpesan.focus();
		eobj.innerHTML=error;
		return false;
	}
	return true;
}


function validPhone(form)/* phone no validation */{         
	var eobj=document.getElementById('phonenoerror');
	var valid = '0123456789';
	var phone = form.txtnotelp.value;
	var error=false;
	var i=0;
	var temp;
	eobj.innerHTML='';
	if (phone == '')
	{
		error='No Telpon tidak boleh kosong';
	}
	else if (!phone.length > 1 || phone.length < 7)
	{
		error='Nomor telpon anda terlalu pendek, Isi lagi';
	}
	else
	{
		for (i=0; i < phone.length; i++)
		{
			temp = '' + phone.substring(i, i + 1);
			if (valid.indexOf(temp) == -1)
			{
				error='Tidak boleh ada karakter, Coba lagi';
			}
		}
	}
	if (error)
	{
		form.txtnotelp.focus();
		eobj.innerHTML=error;
		return false;
	}
	return true;
}

function checkEmail(form)/* untuk email validation */{
	var eobj=document.getElementById('emailerror');
	eobj.innerHTML='';
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form.txtemail.value))
	{
		return true;
	}
	eobj.innerHTML='Salah memasukkan E-mail, coba ganti';
	return false;
}

function validate(){
	var form = document.forms['form'];
	var ary=[checkName,checkTglLahir,checkAlamat,validPhone,checkEmail];
	var rtn=true;
	var z0=0;
	for (var z0=0;z0<ary.length;z0++)
	{
		if (!ary[z0](form))
		{
			rtn=false;
		}
	}

	return rtn;
}

function validate2(){
	var form = document.forms['form'];
	var ary=[checkName, checkEmail, checkPesan];
	var rtn=true;
	var z0=0;
	for (var z0=0;z0<ary.length;z0++)
	{
		if (!ary[z0](form))
		{
			rtn=false;
		}
	}

	return rtn;
}
</script>



</head>
<body>
<?php 


if ($_GET['subid'] == "" ){
	$idnews = 0;
}else{
	$idnews = $_GET['subid'];
}
if ($_GET['news'] == "" ){
	$news = 0;
}else{
	$news = $_GET['news'];
}

if (($tipe == 3 && $news == 7) && ( $idnews <> ""))
{

		$query = "SELECT * FROM t_news WHERE subtipe = '".$news."' AND id_news = '".$idnews."' Order by id_news DESC";
		$data2 = $database->getData($query);
		//echo $query ;
		echo '<div id="contentisi" style="background:url(../images/bgkiri.jpg)">';
		if(!empty($data2))
		{
		//'.$data2[0]['visi'].'
		echo '<h2 style="padding:15px 0 0 15px; ">'.$data2[0]['vjudul'].'</h2>';
		//echo '<p>'.str_replace("\r\n","<br>",$data2[0]['visi']).'</p>';
			echo str_replace("<p","<p class='fullbg'",$data2[0]['visi']);
		}else{
			echo '<h2>Belum Ada Data</h2>';
		}
	echo '</div>';
}else{
	
?>

<div id="contentisi" >
	<?php
	$query = "SELECT * FROM t_banner WHERE id_subtipe = 5 ";
	$data = $database->getData($query);
	//echo $query;
	if(!empty($data))
	{	
		$vvcolor ='style="color:'.$data[0]['vcolor'].';"';
        	$vimages= 'style="background:url(../admin/images/kiri/'.$data[0]['file_name'].');"';
	}
	?>
	
	<div class="wrapkanan" <?php echo $vimages?>>
    	<?php
        require_once("menukanan.php");
		?>
       <!-- <div style="display:inline-block;color:#F00;font-size:24px;margin-top:20px;text-align:center;width:100%;">1 <span style="color:#060">2</span> 3 4 5</div>-->
                   <ul style="margin-top:0px;">
        	<?php  
			if($tip == 1){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=1" class="select2"><span class="bingkai">Pondok Renungan</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=1" '.(($news==1)?'class="select"':'').' title="Reflection">Reflection</a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=2" '.(($news==2)?'class="select"':'').' title="Words Of Wisdom">Words Of Wisdom</a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=3" '.(($news==3)?'class="select"':'').' title="Retreat">Retreat</a></li>';
			}elseif ($tipe == 2){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=2" class="select2"><span class="bingkai">Pondok Doa</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=2&amp;news=4" '.(($news==4)?'class="select"':'').' title="Yesus Kristus">Yesus Kristus</a></li>';
				echo '<li><a href="../news/index.php?tipe=2&amp;news=5" '.(($news==5)?'class="select"':'').' title="Bunda Maria">Bunda Maria</a></li>';
				echo '<li><a href="../news/index.php?tipe=2&amp;news=6" '.(($news==6)?'class="select"':'').' title="Santo / Santa">Santo / Santa</a></li>';
			}elseif ($tipe == 3){
				//echo '<li style="width:100%;"><a href="../news/index.php?tipe=3" class="select2"><span class="bingkai">Pondok Anak</span><span class="bingkairight">&nbsp;</span></a></li>';
				//echo '<li><a href="../news/index.php?tipe=3&amp;news=7" '.(($news==7)?'class="select"':'').' title="Kisah">Kisah</a></li>';
				//echo '<li><a href="../news/index.php?tipe=3&amp;news=8" '.(($news==8)?'class="select"':'').' title="Bermain Bersama">Bermain Bersama</a></li>';
				//echo '<li><a href="../news/index.php?tipe=3&amp;news=9" '.(($news==9)?'class="select"':'').' title="Sahabat">Sahabat</a></li>';
			}elseif ($tipe == 4){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=4" class="select2"><span class="bingkai">Pondok Kita</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=10" '.(($news==10)?'class="select"':'').' title="Info">Info</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=11" '.(($news==11)?'class="select"':'').' title="Musik">Musik</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=12" '.(($news==12)?'class="select"':'').' title="Buku">Buku</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=13" '.(($news==13)?'class="select"':'').' title="Film">Film</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=14" '.(($news==14)?'class="select"':'').' title="Benda Rohani">Benda Rohani</a></li>';
				echo '<li><a href="../news/index.php?tipe=4&amp;news=15" '.(($news==15)?'class="select"':'').' title="Lain-lain">Lain-lain</a></li>';
			}elseif ($tipe==5){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=5" class="select2"><span class="bingkai">Papan Pengumuman</span><span class="bingkairight">&nbsp;</span></a></li>';
			}elseif ($tipe==6){
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=6" class="select2"><span class="bingkai">Dari Aku Untuk Kamu</span><span class="bingkairight">&nbsp;</span></a></li>';
			}elseif ($tipe==7){
				//echo '<li style="width:100%;"><a href="../news/index.php?tipe=7" class="select2"><span class="bingkai">Member</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '';
			}elseif ($tipe==8){
				//echo '<li style="width:100%;"><a href="../news/index.php?tipe=8" class="select2"><span class="bingkai">Contact Us</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '';
			}else{
				echo '<li style="width:100%;"><a href="../news/index.php?tipe=1" class="select2"><span class="bingkai">Pondok Renungan</span><span class="bingkairight">&nbsp;</span></a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=1" '.(($news==1)?'class="select"':'').' title="Reflection">Reflection</a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=2" '.(($news==2)?'class="select"':'').' title="Words Of Wisdom">Words Of Wisdom</a></li>';
				echo '<li><a href="../news/index.php?tipe=1&amp;news=3" '.(($news==3)?'class="select"':'').' title="Retreat">Retreat</a></li>';
			}
			?>        	
        </ul> 
        
        <?php
		$querytipe = "SELECT * FROM t_news WHERE tipe = '".$tipe."' Order by id_news DESC";
		$datatipe = $database->getData($querytipe);
		if(!empty($datatipe))
		{
			if ($news == ""){
				$news = $datatipe[0]['subtipe'];
			}else{
				$news = $_GET['news'];
			}
		}else{
			$news = 0;
		}
		//echo $querytipe;
		
		$query = "SELECT * FROM t_news WHERE subtipe = '".$news."' AND tipe ='".$tipe."' Order by id_news DESC";
		$data = $database->getData($query);
		//echo $query;
		if ($tipe <>3) {
			if(!empty($data))
			{		
				if ($idnews == ""){
					$idnews = $data[0]['id_news'];
				}else{
					$idnews = $_GET['subid'];
				}
				echo '<ul class="list">';
				foreach ($data as $val)
				{
					echo '<li><a href="index.php?tipe='.$tipe.'&amp;news='.$news.'&amp;subid='.$val['id_news'].'" title="'.$val['vjudul'].'" '.(($val['id_news']==$idnews)?'class="select"':'').'>'.$val['vjudul'].'</a></li>';
				}
				echo ' </ul>';
			}else{
				if ($tipe == 8 || $tipe == 7){
					echo '';
				}else{
					echo '<ul class="list"><li>Belum Ada Data</li> </ul>';
				}
				
			}
		}
		
		
		?>
    </div>
    <?php
    
	$query = "SELECT * FROM t_banner WHERE id_subtipe = 2 AND id_tipe ='".$tipe."'";
	$data = $database->getData($query);
	//echo $query;
	if(!empty($data))
	{	
	$vvcolor ='style="color:'.$data[0]['vcolor'].';"';
        $vimages= 'style="background:url(../admin/images/background/'.$data[0]['file_name'].');"';
	}
	//echo $vimages
    ?>
    <div class="wrapkiri" <?php echo $vimages?>>

              
	<?php if ($tipe == 7){ 
        if ($_GET['error'] == 1){
            echo '<strong style="margin-left:16px;color:red;">Data Berhasil Disimpan !!!</strong>';
        }
        ?>
        <div <?php echo $vvcolor?>>
            <form name="form"  method="post" action="savemember.php"  onsubmit="return validate(this)" >
            <h1 style="margin-left:10px;">Member</h1>
            <fieldset style="margin-top:20px;width:100%;">
                <legend>
                <strong>Join Member</strong>
                </legend>
                <table>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><input type="text" name="txtnama" value="" size="44" maxlength="50"> <span id="realnameerror"></span></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td><input name="txtjenis" type="radio" value="pria">Pria <input name="txtjenis" type="radio" value="wanita">Wanita</td>
                    </tr>
                    <tr>
                        <td valign="top">Tempat Tanggal Lahir</td>
                        <td>:</td>
                        <td>
                            <input type="text" name="txttempat" value="" maxlength="20"> 
                            <select name="thxtgl">
                                <?php
                                for($i=1;$i<=31;$i++)
                                {
                                    if ($i <= 9)
                                    {
                                        $no = "0".$i;
                                    }else{
                                        $no = $i;
                                    }
                                    echo "<option value=".$no.">".$no."</option>";
                                }                       
                                ?>	
                            </select>
                            <select name="thxbln">
                                <?php
                                for($i=1;$i<=12;$i++)
                                {
                                    if ($i <= 9)
                                    {
                                        $no = "0".$i;
                                    }else{
                                        $no = $i;
                                    }
                                    echo "<option value=".$no.">".$no."</option>";
                                }                       
                                ?>	
                            </select>
                            <select name="txtthn">
                                <?php
                                for($i=(date("Y")-70);$i<=date("Y");$i++)
                                {
                                    if ($i <= 9)
                                    {
                                        $no = "0".$i;
                                    }else{
                                        $no = $i;
                                    }
                                    echo "<option value=".$no.">".$no."</option>";
                                }                       
                                ?>	
                            </select> <span id="TglLahirerror" ></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><textarea name="txtalamat" cols="37"></textarea> <span id="Alamaterror" ></span></td>
                    </tr>
                    <tr>
                        <td>No Telp</td>
                        <td>:</td>
                        <td><input type="text" name="txtnotelp" value="" size="44" maxlength="20"> <span id="phonenoerror" ></span></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td><input type="text" name="txtemail" value="" size="44" maxlength="50"> <span id="emailerror" ></span></td>
                    </tr>    
                    <tr>
                        <td valign="top">Gambar Kartu</td>
                        <td valign="top">:</td>
                        <td>
                            <div style="display:inline-block;width:180px;height:100px;"><input name="txtkartu" type="radio"  value="kartu1.jpg">Natural<br/><img src="../images/kartu/kartu1.jpg" width="180"/> </div>
                            <div style="display:inline-block;width:180px;height:100px;"><input name="txtkartu" type="radio"  value="kartu2.jpg">Seven Sorrow<br/><img src="../images/kartu/kartu2.jpg"  width="180"/> </div>
                            <div style="display:inline-block;width:180px;height:100px;"><input name="txtkartu" type="radio"  value="kartu3.jpg">Jesus<br/><img src="../images/kartu/kartu3.jpg"  width="180"/> </div>
                        </td>
                    </tr>
                   
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Add"></td>
                    </tr>
                </table>
            </fieldset>
            </form>
            </div>
            <?php
        }elseif ($tipe == 8){
if ($_GET['error'] == 1){
            echo '<strong style="margin-left:16px;color:red;">Data Berhasil Disimpan !!!</strong>';
        }
            ?>
            <div <?php echo $vvcolor?>>
            <form name="form"  method="post" action="savecontact.php" onsubmit="return validate2(this)">
            <h1 style="margin-left:10px;">Contact Us</h1>
            <fieldset style="margin-top:20px;">
                <legend>
                <strong>Contact Us</strong>
                </legend>
                <table>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><input type="text" name="txtnama" value="" size="44" maxlength="50" /> <span id="realnameerror" ></span></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td><input name="txtjenis" type="radio" value="pria" />Pria <input name="txtjenis" type="radio" value="wanita" />Wanita</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td><input type="text" name="txtemail" value="" size="43" maxlength="50" /> <span id="emailerror" ></span></td>
                    </tr> 
                    <tr>
                        <td>Pesan</td>
                        <td>:</td>
                        <td><textarea name="txtpesan" cols="36"></textarea> <span id="Pesanerror" ></span></td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Add" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" ><p style="width:600px;" align="center">"Untuk informasi lebih lanjut dapat menghubungi cs@parkirsejenak.com "</p></td>
                    </tr>
                </table>
            </fieldset>
            </form>
            </div>
            <?php
        }else{

	    if ($tipe == 3){
		echo '<ul>';
            	echo '<li style="width:100%;"><a href="../news/index.php?tipe=3" class="select2"><span class="bingkai">Pondok Anak</span><span class="bingkairight">&nbsp;</span></a></li>';
		echo '<li><a href="../news/index.php?tipe=3&amp;news=7" '.(($news==7)?'class="select"':'').' title="Kisah">Kisah</a></li>';
		echo '<li><a href="../news/index.php?tipe=3&amp;news=8" '.(($news==8)?'class="select"':'').' title="Bermain Bersama">Bermain Bersama</a></li>';
		echo '<li><a href="../news/index.php?tipe=3&amp;news=9" '.(($news==9)?'class="select"':'').' title="Sahabat">Sahabat</a></li>';
		echo '</ul>';

				$querytipe = "SELECT * FROM t_news WHERE tipe = '".$tipe."' Order by id_news DESC";
		$datatipe = $database->getData($querytipe);
		if(!empty($datatipe))
		{
			if ($news == ""){
				$news = $datatipe[0]['subtipe'];
			}else{
				$news = $_GET['news'];
			}
		}else{
			$news = 0;
		}
		//echo $querytipe;
		
		$query = "SELECT * FROM t_news WHERE subtipe = '".$news."' AND tipe ='".$tipe."' Order by id_news DESC";
		$data = $database->getData($query);
		//echo $query;
		if(!empty($data))
		{		
			if ($idnews == ""){
				$idnews = $data[0]['id_news'];
			}else{
				$idnews = $_GET['subid'];
			}
			
			echo '<ul class="list">';
			foreach ($data as $val)
			{
				echo '<li><a href="index.php?tipe='.$tipe.'&amp;news='.$news.'&amp;subid='.$val['id_news'].'" title="'.$val['vjudul'].'" '.(($val['id_news']==$idnews)?'class="select"':'').'>'.$val['vjudul'].'</a></li>';
			}
			echo ' </ul>';
			
		}else{
				echo '<ul class="list"><li>Belum Ada Data</li> </ul>';
			
		}


            }else{
				$query = "SELECT * FROM t_news WHERE subtipe = '".$news."' AND id_news = '".$idnews."' Order by id_news DESC";
            	$data2 = $database->getData($query);
            	//echo $query ;
            	if(!empty($data2))
            	{
                //'.$data2[0]['visi'].'
                echo '<h2>'.$data2[0]['vjudul'].'</h2>';
               	//echo '<p>'.str_replace("\r\n","<br>",$data2[0]['visi']).'</p>';
               	echo '<span '.$vvcolor.'>';
				echo $data2[0]['visi'];
				echo '</span>';
            	}else{
                	echo '<h2>Belum Ada Data</h2>';
            	}
	    }
 
            
            
        }
        
        
        ?>

    </div>
    <!--<div align="center"><a href="#" title="Previous"><img src="../images/prev.png" width="50" /></a> &nbsp;&nbsp; <a href="#" title="Next"><img src="../images/next.png" width="50" /></a></div>-->

</div>
<?php
}
?>
</body>
</html>